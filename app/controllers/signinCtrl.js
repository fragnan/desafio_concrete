var mongoose = require('mongoose'),
    CadastroUsuario = mongoose.model('DesafioConcrete'),
    bcrypt = require('bcryptjs'),
    jwt = require('jwt-simple'),
    moment = require('moment');

//GET Verificar Usuario e senha
exports.signin = function(req, res) {
    CadastroUsuario.findOne({email: req.body.email}, function(err, usuario) {
        if (err) {
            res.status(500).send({'mensagem': 'mensagem de erro'});
        } else {
            if (usuario === null) {
                res.status(400).send('Usuário e/ou senha inválidos');
            } else if (bcrypt.compareSync(req.body.senha, usuario.senha) === false) {
                res.status(401).send('Usuário e/ou senha inválidos');
            } else {
                var dataHora = new Date();
                var token = jwt.encode('novoToken', 'token');
                var data = ({
                    nome: usuario.nome,
                    email: usuario.email,
                    senha: usuario.senha,
                    telefones: usuario.telefones,
                    id: usuario.id,
                    data_criacao: usuario.data_criacao,
                    data_atualizacao: usuario.data_atualizacao,
                    ultimo_login: tratamentoDatas(dataHora),
                    horario_ultimoLogin: tratamentoHora(dataHora),
                    token: token
                });
                usuario.update(data, function(err, teste) {
                    if (err) {
                        res.status(500).send({'mensagem': 'mensagem de erro'});
                    } else {
                        res.status(200).jsonp(data);
                    }
                });
            }
        }
    });
};

var tratamentoDatas = function(data) {
    var dataFormatada = moment(data).format('DD/MM/YYYY');
    return dataFormatada;
};

var tratamentoHora = function(data) {
    var horaFormatada = moment(data).format('HH:mm');
    return horaFormatada;
};



