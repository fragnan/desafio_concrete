var mongoose = require('mongoose');
var CadastroUsuario = mongoose.model('DesafioConcrete');
var moment = require('moment');
var uuid = require('uuid');
var bcrypt = require('bcryptjs');

//GET Obtem todos os registros
exports.encontrarCadastros = function(req, res) {
	CadastroUsuario.find(function(err, data) {
		if (err) {
        	res.status(500).send({'mensagem': 'mensagem de erro'});
		} else {
			res.status(200).jsonp(data);
		}
	});
};

//GET obtem usuário pelo ID
exports.encontrarCadastro = function(req, res) {
	CadastroUsuario.findById(req.params.id, function(err, data) {
		if (err) {
            res.status(500).send({'mensagem': 'mensagem de erro'});
		} else {
			res.status(200).jsonp(data);
		}
	});
};

//POST Criar o usuário na base de dados
exports.criarCadastro = function(req, res) {
	var data = new CadastroUsuario({
		nome : req.body.nome,
		email: req.body.email,
		senha: bcrypt.hashSync(req.body.senha),
		telefones: req.body.telefones,
		id: uuid.v1(req.params.id),
		data_criacao: tratamentoDatas(data),
		data_atualizacao: tratamentoDatas(data),
		ultimo_login: tratamentoDatas(data),
		horario_ultimoLogin: tratamentoHora(data),
		token: uuid.v1()
	});
	data.save(function(err, data) {
		if (err) {
			if (err.name === 'MongoError' && err.code === 11000) {
				res.send('E-mail já existente');
			} else {
				res.status(500).send({'mensagem': 'mensagem de erro'});
			}
		} else {
			res.status(200).jsonp(data);
		}
	});
};

var tratamentoDatas = function(data) {
	var dataFormatada = moment(data).format('DD/MM/YYYY');
	return dataFormatada;
};

var tratamentoHora = function(data) {
	var horaFormatada = moment(data).format('HH:mm');
	return horaFormatada;
};
