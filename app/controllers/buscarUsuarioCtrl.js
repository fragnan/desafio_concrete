var mongoose = require('mongoose'),
	CadastroUsuario = mongoose.model('DesafioConcrete'),
	moment = require('moment');


//GET Logar Pelo Token
exports.buscarUsuario = function(req, res) {
	CadastroUsuario.findOne({token: req.headers.token}, function(err, usuario) {
		if (usuario === null) {
			res.status(401).send('Não autorizado');
		} else if (err) {
			res.status(500).send({'mensagem': 'mensagem de erro'});
		} else if (req.headers.token !== usuario.token) {
			res.status(401).send('Não autorizado');
		} else if (tratamentoDiferencaHora(usuario.horario_ultimoLogin) < 30) {
			res.status(401).send('Sessão inválida');
		} else {
			res.jsonp({
				data: usuario
			});
		}
	});
};

var tratamentoHora = function(data) {
	var horaFormatada = moment(data).format('HH:mm');
	return horaFormatada;
};

var tratamentoDiferencaHora = function(diferenca) {
	var atual_login = new Date();
	var ultimo_login = diferenca;
	var data1 = moment(ultimo_login, "hh:mm");
    var data2 = moment(atual_login, "hh:mm");
    diferenca = data1.diff(data2, 'minutes');
    return diferenca;
};