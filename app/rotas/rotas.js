var express = require('express'),
	rotasDesafioConcrete = express.Router(),
	DesafioConcreteCtrl = require('../controllers/desafioConcreteCtrl.js'),
	SigninCtrl = require('../controllers/signinCtrl.js'),
	BuscarUsuarioCtrl = require('../controllers/buscarUsuarioCtrl.js');

rotasDesafioConcrete.route('/cadastro')
	.get(DesafioConcreteCtrl.encontrarCadastros)
	.post(DesafioConcreteCtrl.criarCadastro);

rotasDesafioConcrete.route('/cadastro/:id')
	.get(DesafioConcreteCtrl.encontrarCadastro);

rotasDesafioConcrete.route('/signin')
	.put(SigninCtrl.signin);

rotasDesafioConcrete.route('/buscarsuario')
	.get(BuscarUsuarioCtrl.buscarUsuario);

module.exports = rotasDesafioConcrete;
