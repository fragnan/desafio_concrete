exports = module.exports = function(app, mongoose) {
	var cadastroModelSchema = new mongoose.Schema({
		nome:  { type: String },
		email: { type: String, unique: true },
		senha: { type: String },
		telefones: [{
			ddd: { type: Number },
			numero:	{ type: Number }
		}],
		id: 			  { type: String },
		data_criacao: 	  { type: String },
		data_atualizacao: { type: String },
		ultimo_login: 	  { type: String },
		horario_ultimoLogin: { type: String },
		token: 		  	  { type: String }
	});
	mongoose.model('DesafioConcrete', cadastroModelSchema);
};