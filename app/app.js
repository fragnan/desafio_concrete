var express = require('express'),
	app = express(),
	bodyParser = require('body-parser'),
	methodOverride = require('method-override'),
	dbConfig = require('./db/db.js'),
	mongoose = require('mongoose');
	
mongoose.connect(dbConfig.url);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});

var models = require('./models/desafioConcreteModel.js')(app, mongoose);
var index = require('./rotas/rotas.js'); 

app.use('/', index);
app.listen(3000, function() {
	console.log('Aplicação rodando na porta http://localhost:3000');
});